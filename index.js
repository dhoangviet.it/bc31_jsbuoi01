/*Bai tap 1 */

/*
 Input: Lương 1 ngày và số ngày làm

 Lương 1 ngày =10000;
 Số ngày làm  =25;

 Các bước xử lý
 B1:
    Tạo 2 biến:
    luong1ngay và songaylam
B2: Tạo biến Tổng tiền Lương(tongtienluong)
    Áp dụng công thức:
    tongtienluong = songaylam*luong1ngay;
    
 Output: 
 Tính tiền lương =2.500.000;

*/

var luongMotNgay, soNgayLam;

luongMotNgay = 100000;

soNgayLam = 25;

var tongTien = Math.floor(luongMotNgay * soNgayLam);

console.log("Tổng tiền lương:", new Intl.NumberFormat().format(tongTien)  +"VNĐ")

/*Bai tap 2 */
/*
 Input: 
    num1= 1;
    num2= 2;
    num3= 3;
    num4= 4;
    num5= 5;


 Output: 
 Tổng giá trị trung bình của 5 số thực là: 3

*/

var num1 ,num2,num3,num4,num5;

num1= 1;
num2= 2;
num3= 3;
num4= 4;
num5= 5;

var sum = Math.floor((num1 + num2 +num3 +num4 +num5)/5);

console.log("Tổng giá trị trung bình của 5 số thực là:", sum);

/*Bai tap 2 */
/*
 Input: 
    USD =2;
    VND =23500;


 Output: 
 Quy đổi từ USD sang :  47.000VNĐ

*/

var USD ,VND;

USD =2;
VND =23500;

var result = USD*VND;

 console.log("Quy đổi từ USD sang : ", new Intl.NumberFormat().format(result) +"VNĐ");

 /*Bai Tap 4 */
/*
  Input: 
    edge1 = 5;
    edge2 = 4;
Các bước xử lý


  S = Math.floor(edge1*edge2);

  P =Math.floor((edge1+edge2)*2)

 Output: 
 Diện tích hình chữ nhật là:  20
 Chu vi hình chữ nhật là:  18

*/

  var edge1, edge2 , S ,P;

  edge1 = 5;
  edge2 = 4;

  S = Math.floor(edge1*edge2);

  P = Math.floor((edge1+edge2)*2)

  console.log("Diện tích hình chữ nhật là: ",S);
  console.log("Chu vi hình chữ nhật là: ",P);

  /*Bai Tap 5 */
/*
khai bao bien:
    n ,
    unit,
    ten,
    sum;
  Input: 
   n = 44;
Các bước xử lý

    ten = Math.floor(n/10);
    unit = n%10;
    sum= ten + unit;

 Output: 
Tổng 2 ký số vừa nhập là: 8

*/



var n , unit, ten, sum;
    n = 44;
    ten = Math.floor(n / 10);
    unit = n % 10;

    sum= ten + unit;
    console.log("Tổng 2 ký số vừa nhập là:", sum);

